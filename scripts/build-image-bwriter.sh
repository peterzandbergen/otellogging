#!/bin/bash

CMD=podman
CMD=docker

SCRIPTDIR=$(dirname ${BASH_SOURCE[0]})
TAG=docker.io/peterzandbergen/bwriter:latest

$CMD build \
    --tag bwriter \
    --tag $TAG \
    --file ${SCRIPTDIR}/../docker/bwriter/Dockerfile ${SCRIPTDIR}/..

$CMD push $TAG