#!/bin/bash

CMD=podman
CMD=docker

SCRIPTDIR=$(dirname ${BASH_SOURCE[0]})
TAG=docker.io/peterzandbergen/alpine-debug:latest

$CMD build \
    --tag loggen \
    --tag $TAG \
    --file ${SCRIPTDIR}/../docker/debug/Dockerfile ${SCRIPTDIR}/..

$CMD push $TAG