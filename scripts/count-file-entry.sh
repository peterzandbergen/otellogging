#!/bin/bash

DIR=$(dirname ${BASH_SOURCE[0]})

# cat $1 | jq --from-file $DIR/count-file-entry.jq

jq '.resourceLogs[].scopeLogs[].logRecords[].attributes' $1 | grep 'file.entry' | wc