#!/bin/bash

KCMD=kubectl
SCRIPTDIR=$(dirname ${BASH_SOURCE[0]})

(
    cd $SCRIPTDIR/..
    kustomize build kubernetes/apps/loggenerator | $KCMD delete -f -
)