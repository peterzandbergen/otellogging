#!/bin/bash

# $1 is the name of the image source
# $2 is the name of the image destination

function pull_image() {
    docker pull $1
}

function retag_image() {
    docker tag $1 $2
}

function push_image() {
    docker push $1
}

pull_image $1
retag_image $1 $2
push_image $2

