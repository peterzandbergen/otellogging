#!/bin/bash

SCRIPTDIR=$(dirname ${BASH_SOURCE[0]})
TAG=docker.io/peterzandbergen/loggen:latest

docker build \
    --tag loggen \
    --tag $TAG \
    --file ${SCRIPTDIR}/../docker/loggen/Dockerfile ${SCRIPTDIR}/..

docker push $TAG