#!/bin/bash

# $1 must contain the name of the environment

OVERLAY=$1
DIR=$(dirname ${BASH_SOURCE[0]})

KCTL=oc # Change to oc for BD
# KCTL=kubectl # Change to oc for BD
# KCTL=cat # Change to oc for BD

SELECTOR=app=bwriter

# Delete the job
$KCTL delete job --selector=$SELECTOR 2>&1>/dev/null

# Deploy the job and the pvc, mute output
(kustomize build ${DIR}/../kubernetes/apps/bwriter/overlays/${OVERLAY} | ${KCTL} apply -f -) 1>&2 > /dev/null

# Wait for the job to finish, mute output
($KCTL wait --for=condition=complete job --selector=$SELECTOR) 1>&2> /dev/null

# Read the logs
$KCTL logs --selector=$SELECTOR
