# filelog test

This small project tests log rotation with the filelog receiver.
The goal is to check whether the rotation causes dropped loglines.

Components
- program that produces logfiles with structured log entries with order numbers
- otel collector config with filelog receiver and storage config

## Kubernetes test

- run otel collector with filelog receiver and 

