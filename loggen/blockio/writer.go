package blockio

import (
	"crypto/rand"
	"errors"
	"fmt"
	"io"
)

var (
	ErrLimitReached = errors.New("limit reached")
)

type LimitedWriter struct {
	W io.Writer
	N int64
}

func (lw *LimitedWriter) Write(b []byte) (int, error) {
	if lw.N == 0 {
		return 0, ErrLimitReached
	}
	n := len(b)
	if lw.N < int64(n) {
		n = int(lw.N)
	}
	n, err := lw.W.Write(b[:n])
	if err != nil {
		return n, err
	}
	lw.N -= int64(n)
	return n, nil
}

func WriteNBytes(w io.Writer, n int64) (written int64, err error) {
	const size = 1024 * 1024

	return WriteNBytesSize(w, n, size)
}

func WriteNBytesSize(w io.Writer, n int64, blocksize int) (written int64, err error) {
	var b = make([]byte, blocksize)
	_, err = rand.Read(b)
	if err != nil {
		return 0, fmt.Errorf("error filling block with random data: %w", err)
	}
	return WriteNBytesBuffer(w, n, b)
}

func WriteNBytesBuffer(w io.Writer, n int64, b []byte) (written int64, err error) {
	bw := &LimitedWriter{
		W: w,
		N: n,
	}
	for n, err := bw.Write(b); ; n, err = bw.Write(b) {
		if errors.Is(err, io.EOF) || errors.Is(err, ErrLimitReached) {
			// this is ok
			return written, nil
		}
		if err != nil {
			return 0, fmt.Errorf("error writing bytes: %w", err)
		}
		written += int64(n)
	}
}

type CountingWriter struct {
	W io.Writer
	N int64
}

func (w *CountingWriter) Write(b []byte) (int, error) {
	n, err := w.W.Write(b)
	w.N += int64(n)
	return n, err
}
