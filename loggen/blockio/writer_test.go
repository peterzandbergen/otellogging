package blockio

import (
	"bytes"
	"reflect"
	"testing"
)

func TestWriteNBytes(t *testing.T) {
	tf := func(n int64) (int64, error) {
		b := bytes.Buffer{}
		_, err := WriteNBytes(&b, n)
		if err != nil {
			return 0, err
		}
		return int64(b.Len()), nil
	}

	type args struct {
		n int64
	}
	type result struct {
		n   int64
		err error
	}
	cases := []struct {
		name     string
		args     args
		want     result
		validate func(t *testing.T, want, got result)
	}{
		{
			name: "small file",
			args: args{n: 100},
			want: result{n: 100, err: nil},
			validate: func(t *testing.T, want, got result) {
				if !reflect.DeepEqual(want, got) {
					t.Fatalf("want %v, got %v", want, got)
				}
			},
		},
		{
			name: "large file 100000",
			args: args{n: 100000},
			want: result{n: 100000, err: nil},
			validate: func(t *testing.T, want, got result) {
				if !reflect.DeepEqual(want, got) {
					t.Fatalf("want %v, got %v", want, got)
				}
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var got result
			got.n, got.err = tf(c.args.n)
			c.validate(t, c.want, got)
		})
	}
}
