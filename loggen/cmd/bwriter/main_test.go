package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestMain(t *testing.T) {
	args := []string{
		"-logjson=false",
		"-filename", "baggerfile",
		"-filesize", "100",
	}
	initAndRun(args)
}

func TestWriteFile(t *testing.T) {
	dir := t.TempDir()

	name := filepath.Join(dir, "tempfile.out")

	var size = int64(10000000)
	err := writeFile(name, 10000000, 100)
	if err != nil {
		t.Fatalf("error: %s", err)
	}

	// Check the file size.
	fi, err := os.Stat(name)
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if fi.Size() != size {
		t.Fatalf("size differs, got %d, want %d", fi.Size(), size)
	}
}
