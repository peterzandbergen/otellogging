package main

import (
	"flag"
	"fmt"
	"io"
	"loggen/blockio"
	"os"
	"time"

	"golang.org/x/exp/slog"
)

// config contains the settings for loggen
type config struct {
	filename  string
	filesize  int64
	blocksize int
	logjson   bool
	cleanup   bool
}

func parseConfig(args []string) (*config, error) {
	cfg := &config{}
	f := flag.NewFlagSet("parameters", flag.ExitOnError)
	f.StringVar(&cfg.filename, "filename", "bwrite-file.bin", "file to write, directory needs to exist")
	f.Int64Var(&cfg.filesize, "filesize", 1024*1024, "size of the file to write")
	f.IntVar(&cfg.blocksize, "blocksize", 1024, "size of blocks to write")
	f.BoolVar(&cfg.logjson, "logjson", false, "log in json format")
	f.BoolVar(&cfg.cleanup, "cleanup", true, "cleanup testfile, unset with -cleanup=false")

	if err := f.Parse(args); err != nil {
		return nil, fmt.Errorf("error parsing flags: %w", err)
	}

	if len(f.Args()) > 0 {
		slog.Default().Info("remaining arguments")
	}

	return cfg, nil
}

func initLogger(w io.Writer, json bool) {
	var h slog.Handler
	switch json {
	case true:
		h = slog.NewJSONHandler(w, nil)
	case false:
		h = slog.NewTextHandler(w, nil)
	}
	logger := slog.New(h)
	slog.SetDefault(logger)
}

func writeFile(name string, size int64, blocksize int) error {
	const (
		om = os.O_RDWR | os.O_CREATE | os.O_TRUNC
		fm = 0644 // -rw-r--r  User rw, group r, other r
	)

	f, err := os.OpenFile(name, om, fm)
	if err != nil {
		return fmt.Errorf("error opening file %s: %w", name, err)
	}
	defer f.Close()

	_, err = blockio.WriteNBytes(f, size)
	return err
}

func initAndRun(args []string) {
	initLogger(os.Stdout, false)

	logger := slog.Default()

	cfg, err := parseConfig(args)
	if err != nil {
		logger.Error("error parsing commandline",
			slog.String("error", err.Error()),
			slog.String("filename", cfg.filename),
		)
		return
	}
	if cfg.logjson {
		initLogger(os.Stdout, true)
		logger = slog.Default()
	}
	start := time.Now()
	err = writeFile(cfg.filename, cfg.filesize, cfg.blocksize)
	interval := time.Since(start)

	if err != nil {
		logger.Error("error writing file",
			slog.String("error", err.Error()),
			slog.String("filename", cfg.filename),
		)
	}

	logger.Info("file written",
		slog.Duration("duration", interval),
		slog.String("durationString", interval.String()),
		slog.String("filename", cfg.filename),
		slog.Int64("filesize", cfg.filesize),
	)

	if cfg.cleanup {
		logger.Info("removing file",
			slog.String("filename", cfg.filename),
		)
		os.Remove(cfg.filename)
	} else {
		logger.Info("file kept",
			slog.String("filename", cfg.filename),
		)
	}
}

func main() {
	args := os.Args[1:]
	initAndRun(args)
}
