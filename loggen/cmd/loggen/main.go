package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"golang.org/x/exp/slog"
)

// config contains the settings for loggen
type config struct {
	logPath      string
	logPrefix    string
	logExtension string
	nLogFiles    int
	nLogEntries  int
	keyValues    map[string]any
}

func parseConfig(args []string) (*config, error) {
	cfg := &config{}
	f := flag.NewFlagSet("parameters", flag.ExitOnError)
	f.StringVar(&cfg.logPath, "log-path", ".", "path for the log files")
	f.StringVar(&cfg.logPrefix, "log-name", "logfile", "basename for the logfile")
	f.StringVar(&cfg.logExtension, "log-extension", "log", "extension of the logfile")
	f.IntVar(&cfg.nLogFiles, "num-files", 1, "the number of logfiles to generate")
	f.IntVar(&cfg.nLogEntries, "num-entries", 1000, "the number entries per file")

	kv := f.String("attributes", "", "extra attributes in json format, start with @ to specify a file")

	if err := f.Parse(args); err != nil {
		return nil, fmt.Errorf("error parsing flags: %w", err)
	}

	// Unmarshal the kv if set.
	if *kv == "" {
		return cfg, nil
	}

	attrs := *kv
	if (*kv)[0] == '@' {
		b, err := os.ReadFile(attrs[1:])
		if err != nil {
			return nil, fmt.Errorf("error reading attributes from file: %w", err)
		}
		attrs = string(b)
	}

	if err := json.Unmarshal([]byte(attrs), &cfg.keyValues); err != nil {
		return nil, fmt.Errorf("error parsing attributes: %w", err)
	}
	return cfg, nil
}

func fileExists(name string) bool {
	info, err := os.Stat(name)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir() && info.Mode().IsRegular()
}

func timestamp(t time.Time) string {
	return strconv.FormatUint(uint64(t.UnixNano()), 10)
}

func filePath(path, name, extension string) string {
	return filepath.Join(path, name) + "." + extension
}

func backupFile(path, name, extension string) (filename string, err error) {
	fullPath := filePath(path, name, extension)
	// Test if file exists.
	if !fileExists(fullPath) {
		return fullPath, nil
	}
	newName := filePath(path, strings.Join([]string{name, timestamp(time.Now())}, "-"), extension)
	err = os.Rename(fullPath, newName)
	if err != nil {
		return "", err
	}
	return newName, nil
}

func createFile(path, prefix, extension string) (*os.File, error) {
	const (
		om = os.O_RDWR | os.O_CREATE | os.O_APPEND
		fm = 0644 // -rw-r--r  User rw, group r, other r
	)

	// Backup existing file.
	_, err := backupFile(path, prefix, extension)
	if err != nil {
		return nil, err
	}

	fullPath := filePath(path, prefix, extension)
	f, err := os.OpenFile(fullPath, om, fm)
	if err != nil {
		return nil, fmt.Errorf("error opening file %s: %w", fullPath, err)
	}
	return f, nil
}

func run(n int, cfg *config) error {
	lf, err := createFile(cfg.logPath, cfg.logPrefix, cfg.logExtension)
	if err != nil {
		return err
	}
	defer lf.Close()

	initLogger(lf)

	logger := slog.Default().With("run", n)
	if cfg.keyValues != nil && len(cfg.keyValues) > 0 {
		logger = logger.With(slog.Any("external-values", cfg.keyValues))
	}

	// Generate log entries.
	for i := 0; i < cfg.nLogEntries; i++ {
		// time.Sleep(1000 * time.Millisecond)
		id := 10*cfg.nLogEntries + i
		logger.Info("running",
			slog.Int("id", id),
			slog.Int("file.entry", i),
		)
	}
	return nil
}

func initLogger(w io.Writer) {
	logger := slog.New(slog.NewJSONHandler(w, nil))
	slog.SetDefault(logger)
}

func initAndRun(args []string) {
	cfg, err := parseConfig(args)
	if err != nil {
		log.Fatalf("cannot parse commandline: %s", err)
	}
	_ = cfg
	for i := 0; i < cfg.nLogFiles; i++ {
		if err := run(i, cfg); err != nil {
			log.Fatalf("run failed: %s", err)
		}
		log.Print("run succeeded")
	}
	fmt.Println("done")
}

func main() {
	args := os.Args[1:]
	initAndRun(args)
}
