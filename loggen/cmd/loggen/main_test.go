package main

import (
	"reflect"
	"testing"
)

func TestFlags(t *testing.T) {
	cases := []struct {
		name string
		args []string
		want *config
	}{
		{
			name: "empty",
			args: []string{},
			want: &config{
				logPath: ".",
				logPrefix: "logfile",
				logExtension: "log",
				nLogFiles: 5,
				nLogEntries: 1000,
			},
		},
		{
			name: "logpath",
			args: []string{
				"-log-path", "log-path",
			},
			want: &config{
				logPath: "log-path",
				logPrefix: "logfile",
				logExtension: "log",
				nLogFiles: 5,
				nLogEntries: 1000,
			},
		},
		{
			name: "key values",
			args: []string{
				"-attributes", `{"key":"value"}`,
			},
			want: &config{
				logPath: ".",
				logPrefix: "logfile",
				logExtension: "log",
				nLogFiles: 5,
				nLogEntries: 1000,
				keyValues: map[string]any{
					"key": "value",
				},
			},
		},
		{
			name: "key values from file",
			args: []string{
				"-attributes", "@/home/peza/DevProjects/otellogging/loggen/resources/attributes.json",
			},
			want: &config{
				logPath: ".",
				logPrefix: "logfile",
				logExtension: "log",
				nLogFiles: 5,
				nLogEntries: 1000,
				keyValues: map[string]any{
					"key": "file",
				},
			},
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got, err := parseConfig(c.args)
			if err != nil {
				t.Fatalf("parseConfig failed: %s", err)
			}
			if !reflect.DeepEqual(c.want, got) {
				t.Fatalf("want: %#v, got: %#v", c.want, got)
			}
		})
	}
}
